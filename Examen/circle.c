#include <stdio.h>
#include<stdlib.h>


double perimetre(double rayon) {
    return 2 * 3.14159 * rayon;
}

double surface(double rayon) {
    return 3.14159 * rayon * rayon;
}

int main() {
    double rayon;
    printf("Quelle est le rayon du cercle : ");
    scanf("%lf", &rayon);
    printf("Le perimetre du cercle de rayon %.2f est de : %.2f\n", rayon, perimetre(rayon));
    printf("La surface du disque de rayon %.2f est de : %.2f\n", rayon, surface(rayon));

    return 0;
}

