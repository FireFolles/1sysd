#include<stdio.h>
#include<stdlib.h>

int factorial(int n) {
    int resultat = 1;
    for (int i = 1; i <= n; i++) {
        resultat *= i;
    }
    return resultat;
}

int main(int argc, char *argv[]) {
    int debut, fin;

    if (argc != 3) {
        printf("Erreur\n");
        return 1;
    }
    debut= atoi(argv[1]); 
    fin = atoi(argv[2]);
    for (int i = debut; i <= fin; i++) {
        printf("%d! = %d\n", i, factorial(i));
    }

    return 0;
}
