#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int main() {
    int guess;
    int secret;
    int essai=0;
    srand(time(NULL));
    secret = rand() % 100 + 1 ;
    printf("J'ai tiré un nombre au hasard entre 1 et 100\n");
    while (guess != secret){
    	essai++;
        printf("Votre proposition : ");
        scanf("%d", &guess);
        if (guess == secret) {
            printf("Trouvé!\nTu as mis %d essais\n",essai);
        } else if (guess > secret) {
            printf("Plus petit...\n");
        } else {
            printf("Plus grand...\n");
        }
    }
    return 0;
}



